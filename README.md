
**Note: This project is still in progress, but welcome for any issues**

This repository is developed upon the following tools:

- [CodeIgniter](http://www.codeigniter.com/) (v3.1.13) - PHP framework
- [Bootstrap](http://getbootstrap.com/) (v4.1.x) - Popular frontend
- [BladeOne](https://github.com/EFTEC/BladeOne/) - feature-rich library engine blade view template
- [Route Statics](https://github.com/Patroklo/codeigniter-static-laravel-routes) - library route statics
- [HMVC Module](https://github.com/N3Cr0N/CodeIgniter-HMVC) - Module create HMVC Codeigniter
- [NiceAdmin](https://www.wrappixel.com/templates/niceadmin/#demos) (v2.3.8) - bootstrap theme for Admin Panel


## Server Environment

Below configuration are preferred; other environments are not well-tested, but still feel free to report and issues.

- **PHP 7.4+**
- **Apache 2.2+**
- **MySQL 5.7+**


