<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends MY_Controller {
	private $data=[];
	public function index()
	{
		$this->data['title'] = 'Ini Home';
		$this->view('website', $this->data);
	}

}

/* End of file Website.php */
/* Location: ./application/controllers/Website.php */