<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Captcha extends MY_Controller {

	private $bil1;
	private $bil2;
	private $operator;

	public function __construct()
	{
		parent::__construct();
		$listoperator = array('+');
		$this->bil1 = rand(10,50);
		$this->bil2 = rand(10,50);
		$this->operator = '+';
	}


	function generatekode()
	{

		$hasil = $this->bil1 + $this->bil2;
		$array = array(
			'kode' => $hasil
		);

		$this->session->set_userdata( $array );     
	}

	function showcaptcha()
	{
		$this->generatekode();
		$result = "hasil dari ".$this->bil1." ".$this->operator." ".$this->bil2." ? ";

		$response = [
			'status'=>true,
			'message'=>"OK",
			'data'=>$result
		];
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}  

	function resultcaptcha()
	{
		$this->output->set_content_type('application/json')->set_output(json_encode($this->session->userdata('kode')));
	}

}

/* End of file Captcha.php */
/* Location: ./application/modules/authentication/controllers/Captcha.php */