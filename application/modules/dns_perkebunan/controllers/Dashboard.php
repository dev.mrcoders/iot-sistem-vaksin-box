<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	private $data=[];
	private $nip;

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Home';
		$this->view('index', $this->data);	
	}

	
}

/* End of file Samples.php */
/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */