<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kepemilikan extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_kepemilikan');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Kepemilikan';
		$this->view('kepemilikan.index', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->M_kepemilikan->get_datatables();
		$data = array();
		$no = $this->input->post('start');
			//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no']=$no;
			$row['id_kepemilikan'] = $r->id_kepemilikan;
			$row['kepemilikan'] = $r->kepemilikan;
			$row['keterangan'] = $r->keterangan;

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_kepemilikan->count_all(),
			"recordsFiltered" => $this->M_kepemilikan->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));

	}

	public function GetDataById()
	{

		$get_data_kepemilikan = $this->M_kepemilikan->GetById();

		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$get_data_kepemilikan
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Create()
	{
		$post=$this->input->post();

		if($this->input->post('id_kepemilikan')){
			$this->M_kepemilikan->Updated($post);
		}else{
			$this->M_kepemilikan->Saved($post);
		}


		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$post
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$sid = $this->input->post('sid');

		$this->M_kepemilikan->Deleted($sid);
		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$sid
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}


}

/* End of file Samples.php */
	/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */