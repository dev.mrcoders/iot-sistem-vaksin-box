<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap extends CI_Model {
	var $disbun_db;
	var $table = 'v_rekap_data';
	var $primary_key= 'id_transaksi_rekap';
	var $column_order = array(null,'id_kecamatan','nama_komoditi','kepemilikan','keterangan','semester','tahun','total_luas_lahan','total_produksi');
	var $column_search = array('id_kecamatan','nama_komoditi','kepemilikan','keterangan','semester','tahun','total_luas_lahan','total_produksi');
	var $order = array('id_transaksi_rekap' => 'asc');

	public function __construct()
	{
		parent::__construct();
		$this->disbun_db = $this->load->database('disbun',TRUE);
	}

	private function _get_datatables_query()
	{
		$this->disbun_db->from($this->table);
		if($this->input->post('kecamatan')){
			$this->disbun_db->where('id_kecamatan',$this->input->post('kecamtan'));
		}
		if($this->input->post('periode')){
			$this->disbun_db->where('id_periode',$this->input->post('periode'));
		}
		if($this->input->post('kepemilikan')){
			$this->disbun_db->where('id_kepemilikan',$this->input->post('kepemilikan'));
		}
		if($this->input->post('komoditi')){
			$this->disbun_db->where('id_komoditi',$this->input->post('komoditi'));
		}
		$i = 0;
		foreach ($this->column_search as $item) 
		{
			if ($this->input->post('search')['value']) 
			{
				if ($i === 0) 
				{
					$this->disbun_db->group_start();
					$this->disbun_db->like($item, $this->input->post('search')['value']);
				}else{
					$this->disbun_db->or_like($item, $this->input->post('search')['value']);
				}
				if (count($this->column_search) - 1 == $i) 
					$this->disbun_db->group_end();
			}
			$i++;
		}

		if ($this->input->post('order')) {
			$this->disbun_db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->disbun_db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($this->input->post('length') != -1)
			$this->disbun_db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->disbun_db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->disbun_db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->disbun_db->from($this->table);
		return $this->disbun_db->count_all_results();
	}

	public function Saved($data)
	{
		return $this->disbun_db->insert($this->table,$data);
	}

	public function Updated($data)
	{
		$this->disbun_db->where($this->primary_key,$data['id_periode']);
		return $this->disbun_db->update($this->table,['semester'=>$data['semester'],'tahun'=>$data['tahun']]);
	}

	public function GetById($id=null)
	{
		if($this->input->get('sid')){
			$this->disbun_db->where($this->primary_key,$this->input->get('sid'));
		}
		if($id != null){
			$this->disbun_db->where($this->primary_key,$id);
		}

		$trx = $this->disbun_db->get($this->table);
		return ($this->input->get('sid') ? $trx->row():($id != null ? $trx->row():$trx->result()));
	}

	public function GetByYear($data)
	{
		$this->disbun_db->where('tahun',$data['tahun']);
		$this->disbun_db->where('semester',$data['semester']);
		return $this->disbun_db->get($this->table)->row();
	}

	public function Deleted($id)
	{
		$this->disbun_db->where($this->primary_key,$id);
		return $this->disbun_db->delete($this->table);
	}

}

/* End of file M_komoditi.php */
	/* Location: ./application/modules/dns_perkebunan/models/M_komoditi.php */