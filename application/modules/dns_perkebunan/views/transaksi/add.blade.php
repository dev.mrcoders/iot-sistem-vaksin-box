<div id="add-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="title-form"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-add-transaksi" class="form-horizontal r-separator">
				<input type="hidden" name="id_trx" value="{{ $id_trx }}">
				<input type="hidden" name="mode" value="">
				<div class="modal-body">
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kecamatan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="hidden" name="id_kecamatan">
							<select class="form-control"  id="id_kecamatan" disabled="">
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Komoditi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="hidden" name="id_komoditi">
							<select class="form-control" name="id_komoditi" id="id_komoditi" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kepemilikan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="hidden" name="id_kepemilikan">
							<select class="form-control" name="id_kepemilikan" id="id_kepemilikan" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Periode</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="hidden" name="id_periode">
							<select class="form-control" name="id_periode" id="id_periode" disabled="">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TBM Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="tbm_lalu" id="tbm-lalu" placeholder="TBM (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TBM Semester Ini Menjadi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="tbm_ini_to_tm" id="tbm-lalu-to-tm" placeholder="TM (ha)" disabled="">
							<hr>
							<input type="text" class="form-control" name="tbm_ini_to_ttr" id="tbm-lalu-to-ttr" placeholder="TTR (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TM Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="tm_lalu" id="tm-lalu" placeholder="TM (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TM Semester Ini Menjadi TTR</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="tm_ini_to_ttr" id="tm-ini-to-ttr" placeholder="TTR (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TTR Semester Lalu</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="ttr_lalu" id="ttr-lalu" placeholder="TTR (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">TTR Semester Ini Menjadi TBM</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="ttr_ini_to_tbm" id="ttr-ini-to-tbm" placeholder="TBM (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Semester Ini Buka Lahan Baru</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="lahan_baru" id="lahan-baru" placeholder="Lahan baru (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Keadaan Semester Ini</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="tbm_saat_ini" id="tbm-saat-ini" placeholder="TBM (ha)" disabled="">
							<hr>
							<input type="text" class="form-control" name="tm_saat_ini" id="tm-saat-ini" placeholder="TM (ha)" disabled="">
							<hr>
							<input type="text" class="form-control" name="ttr_saat_ini" id="ttr-saat-ini" placeholder="TTR (ha)" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Jumlah</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="jumlah" id="jumlah" placeholder="" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Produksi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="ton" id="ton" placeholder="Ton" disabled="">
							<hr>
							<input type="text" class="form-control" name="rata_rata" id="rata-rata" placeholder="kg/ha/thn" disabled="">
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0" id="input-petani">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Petani KK</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<input type="text" class="form-control" name="petani" id="petani" placeholder="KK" disabled="">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>