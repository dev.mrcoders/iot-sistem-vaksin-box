<div id="add-modal" class="modal fade"  role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah Data Baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<form id="form-add-generate" class="form-horizontal r-separator">
				<div class="modal-body">
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Komoditi</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_komoditi" id="id_komoditi">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Kepemilikan</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_kepemilikan" id="id_kepemilikan">
								
							</select>
						</div>
					</div>
					<div class="form-group row align-items-center m-b-0">
						<label for="inputEmail3" class="col-3 text-right control-label col-form-label">Periode</label>
						<div class="col-9 border-left p-b-10 p-t-10">
							<select class="form-control" name="id_periode" id="id_periode">
								
							</select>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger waves-effect waves-light btn-simpan">Simpan Dan Generate</button>
				</div>
			</form>
		</div>
	</div>
</div>