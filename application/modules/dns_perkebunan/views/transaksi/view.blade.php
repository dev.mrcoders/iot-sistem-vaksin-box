@extends('template.index')
@section('content')
<style>
	table tr th{
		font-size:10px;
		text-align:center;
		
	}
	table tr td{
		font-size:9px;
		text-align:center;
	}

	.break{
		word-wrap:break-word;
		white-space: pre;
	}
	@media (min-width: 992px){
		.modal-lg {
			max-width: 1149px;
		}
	}

	hr{
		margin-top:0.5rem;
	}
	.blockquote, hr {
		margin-bottom: 0.5rem;
	}
	
</style>
<div class="container-fluid">
	<div class="row">
		{{-- <div class="col-sm-12">
			<input type='button' id='btn' value='Print' onclick='printDiv();'>
		</div> --}}
		<div class="col-lg-12" id="print-div">
			<div class="card" >

				<div class="card-body ">
					<div class="row mb-4">
						<div class="col-sm-3">
							<di class="row">
								<div class="col-sm-12">Rincian Data Produksi</div>
							</di>
							<di class="row">
								<div class="col-sm-4">Komoditi</div>
								<div class="col-sm-4" id="nama-komoditi">:</div>
							</di>
							<di class="row">
								<div class="col-sm-4">Kepemilikan</div>
								<div class="col-sm-4" id="nama-kepemilikan">:</div>
							</di>
							<di class="row">
								<div class="col-sm-4">Periode</div>
								<div class="col-sm-6" id="periode">:</div>
							</di>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table nowrap table-bordered" id="tb-data" width="100%">
							<thead class="bg-inverse text-white">
								<tr>
									<th rowspan="2">
										<span class="break">
											No
										</span>
									</th>
									<th rowspan="2">
										<span class="break">
											Kecamatan
										</span>
										
									</th>
									<th rowspan="2">
										<span class="break">
											TBM SM 
											LALU
										</span>
										
									</th>
									<th colspan="2">TBM SM ini Menjadi</th>
									<th rowspan="2">
										<span class="break">
											TM SM 
											LALU
										</span>
										
									</th>
									<th rowspan="2">
										<span class="break">
											TM SM INI 
											Jadi TTR
										</span>
										
									</th>
									<th rowspan="2">
										<span class="break">
											TTR SM 
											LALU
										</span>
										
									</th>
									<th rowspan="2">
										<span class="break">
											TTR SM INI
											Jadi TBM
										</span>
										
									</th>
									<th rowspan="2">
										<span class="break">
											SM INI Buka 
											Lahan Baru
										</span>
										
									</th>
									<th colspan="3">Keadaan SM INI</th>
									<th rowspan="2">
										<span class="break">
											Jumlah
										</span>
										
									</th>
									<th colspan="2">Produksi</th>
									<th rowspan="2">
										<span class="break">
											Petani (KK)
										</span>
										
									</th>
								</tr>
								<tr>
									<th>TM</th>
									<th>TTR</th>
									<th>TBM</th>
									<th>TM</th>
									<th>TTR</th>
									<th>TON</th>
									<th>kg/ha/thn</th>
								</tr>
								<tr>
									@for($i = 1; $i <= 17; $i++)
									<td style="text-align: center;">{{ $i }}</td>
									@endfor
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
@include('transaksi.add')
@include('transaksi.edit')
<script>
	$(function(){
		let id_kecamatan;
		let id_komoditi;
		let id_kepemilikan;
		let id_periode;
		let datas ={};
		let stats ='add';
		let table;
		let id_trx = '{{ $id_trx }}';

		ajaxcsrf();
		Datatables();
		dataKecamatan();
		dataKomoditi()
		dataKepemilikan()
		dataPeriode()
		

		$("select").select2({
			width: '100%',
			theme: 'bootstrap4',
			allowClear: true,
			readonly:true
		});

		function Datatables(is_rakyat=false) {
			table =$('#tb-data').DataTable({
				"language": {
					"url": base_url+"dist/indonesia.json"
				},
				paging: false,
				ordering: false,
				info: false,
				"bFilter": false,
				stateSave: false,
				destroy: true,
				"processing": true,
				"serverSide": true,
				"order": [],
				"ajax": {
					"url": base_url + "dns_perkebunan/transaksi/data-trx-view",
					"type": "POST",
					"data": {
						csrf_hash_name: $('meta[name="csrf-token"]').attr("content"),
					},
					"data": function(data) {
						data.csrf_hash_name = $('meta[name="csrf-token"]').attr("content");
						data.idtrx=id_trx;
					},
					"dataSrc": function(response) {
						$('meta[name="csrf-token"]').attr("content", response.csrf_param);
						return response.data;
					},

				},
				"columns": [{
					data:"no"
				},
				{
					data:{
						id_kecamtan:"id_kecamatan",
						nama_kecamatan:"nama_kecamatan",
						id_trx:"id_trx",
						data_tanaman_ini:"data_tanaman_ini"
					},render:function(d){
						let btn = '';
						if(d.data_tanaman_ini.tbm == 0){
							btn = '<button class="btn btn-xs btn-info add" data-id="'+d.id_trx+'" data-kecamatan="'+d.id_kecamatan+'">Tambah Data</button>'
						}else{
							btn = '<button class="btn btn-xs btn-warning edit" data-id="'+d.id_trx+'" data-kecamatan="'+d.id_kecamatan+'">Ubah Data</button>'
						}

						return '<span style="font-size:12px;">'+d.nama_kecamatan+'</span> <br>'+btn
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.tbm_lalu != '' ? data.tbm_lalu: 0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.tbm_lalu_to_tm != '' ? data.tbm_lalu_to_tm:0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.tbm_lalu_to_ttr != '' ? data.tbm_lalu_to_ttr:0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.tm_lalu != '' ? data.tm_lalu: 0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.tm_lalu_to_ttr != '' ? data.tm_lalu_to_ttr: 0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.ttr_lalu != '' ? data.ttr_lalu:0)
					}
				},
				{
					data:"data_tanaman_lalu",
					render:function(data){
						return (data.ttr_lalu_to_tbm != '' ? data.ttr_lalu_to_tbm:0)
					}
				},
				{
					data:"data_lahan",
					render:function(data){
						return (data.luas != '' ? data.luas: 0)
					}
				},
				{
					data:"data_tanaman_ini",
					render:function(data){
						return (data.tbm != '' ? data.tbm : 0)
					}
				},
				{
					data:"data_tanaman_ini",
					render:function(data){
						return (data.tm != '' ? data.tm : 0)
					}
				},
				{
					data:"data_tanaman_ini",
					render:function(data){
						return (data.ttr != '' ? data.ttr : 0)
					}
				},
				{
					data:"data_lahan",
					render:function(data){
						return (data.total != '' ? data.total : 0)
					}
				},
				{
					data:"data_produksi",
					render:function(data){
						return (data.produksi != '' ? data.produksi:0)
					}
				},
				{
					data:"data_produksi",
					render:function(data){
						return (data.rata_rata != '' ? data.rata_rata:0)
					}
				},
				{
					visible:(is_rakyat? false:true),
					data:"data_petani",
					render:function(data){
						return (data.petani != '' ? data.petani:0)
					}
				},
				
				],
				
				
			});
		}

		$('.add').on('click',function(){
			stats ='add';
			$('#form-add-transaksi')[0].reset();
			$('#add-modal').modal('show')
		})

		$('#tb-data').on('click','.add',function(){
			let sid = $(this).data('id');
			let kec = $(this).data('kecamatan');
			$('#title-form').text('Tambah Data Baru');
			$('[name="mode"]').val('add');
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/databyid',
				type: 'get',
				data: {
					sids:sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function (response) {
					if(response.data.kepemilikan.id_ != 4){
						$('#input-petani').attr('hidden', 'hidden');
					}else{
						$('#input-petani').removeAttr('hidden');
					}
					$('#form-add-transaksi')[0].reset();
					datas['id_kecamatan']=kec
					datas['id_komoditi']=response.data.komoditi.id_
					datas['id_kepemilikan']=response.data.kepemilikan.id_
					datas['id_periode']=response.data.periode.id_
					searchDataSmstlalu()
					$('#add-modal').modal('show')
					$('#id_kecamatan').val(kec).change();
					$('[name="id_kecamatan"]').val(kec);
					$('#id_komoditi').val(response.data.komoditi.id_).change();
					$('[name="id_komoditi"]').val(response.data.komoditi.id_);
					$('#id_kepemilikan').val(response.data.kepemilikan.id_).change();
					$('[name="id_kepemilikan"]').val(response.data.kepemilikan.id_);
					$('#id_periode').val(response.data.periode.id_).change();
					$('[name="id_periode"]').val(response.data.periode.id_);

				}
			});
			
			
		})

		$('#tb-data').on('click','.edit',function(){
			let sid = $(this).data('id');
			let kec = $(this).data('kecamatan');
			$('#title-form').text('Ubah Data');
			$('[name="mode"]').val('edit');
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/databyid',
				type: 'get',
				data: {
					sids:sid,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function (response) {
					$('#form-add-transaksi input').removeAttr('disabled');
					if(response.data.kepemilikan.id_ != 4){
						$('#input-petani').attr('hidden', 'hidden');
					}else{
						$('#input-petani').removeAttr('hidden');
					}

					$('#form-add-transaksi')[0].reset();
					datas['id_kecamatan']=kec
					datas['id_komoditi']=response.data.komoditi.id_
					datas['id_kepemilikan']=response.data.kepemilikan.id_
					datas['id_periode']=response.data.periode.id_
					dataTransaksi()
					$('#add-modal').modal('show')
					$('#id_kecamatan').val(kec).change();
					$('[name="id_kecamatan"]').val(kec);
					$('#id_komoditi').val(response.data.komoditi.id_).change();
					$('[name="id_komoditi"]').val(response.data.komoditi.id_);
					$('#id_kepemilikan').val(response.data.kepemilikan.id_).change();
					$('[name="id_kepemilikan"]').val(response.data.kepemilikan.id_);
					$('#id_periode').val(response.data.periode.id_).change();
					$('[name="id_periode"]').val(response.data.periode.id_);
				}
			});
			
			
		})

		$('#tb-data').on('click','.hapus',function(){
			let sid = $(this).data('id');
			
			Swal.fire({
				title: 'Are you sure?',
				text: "Data Tidak Dapat Dipulihkan",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, delete it!'
			}).then((result) => {
				if (result.isConfirmed) {
					deleteTransaksi(sid)
				}
			})
			
		})

		$('#tbm-saat-ini').on('click',function(){
			let tbm_lalu=parseInt($('#tbm-lalu').val()) || 0;
			let tbm_lalu_to_tm=parseInt($('#tbm-lalu-to-tm').val()) || 0;
			let tbm_lalu_to_ttr=parseInt($('#tbm-lalu-to-ttr').val()) || 0;
			let ttr_ini_to_tbm =parseInt($('#ttr-ini-to-tbm').val()) || 0;
			let lahan_baru =parseInt($('#lahan-baru').val()) || 0;

			let tbm_saat_ini = tbm_lalu - tbm_lalu_to_tm - tbm_lalu_to_ttr + ttr_ini_to_tbm + lahan_baru;

			$('#tbm-saat-ini').val(tbm_saat_ini);
			
		})

		$('#tm-saat-ini').on('click',function(){
			let tbm_lalu_to_tm=parseInt($('#tbm-lalu-to-tm').val()) || 0;
			let tm_lalu=parseInt($('#tm-lalu').val()) || 0;
			let tm_ini_to_ttr=parseInt($('#tm-ini-to-ttr').val()) || 0;

			let tm_saat_ini = tbm_lalu_to_tm + tm_lalu - tm_ini_to_ttr;

			$('#tm-saat-ini').val(tm_saat_ini);
		})

		$('#ttr-saat-ini').on('click',function(){
			let tbm_lalu_to_ttr=parseInt($('#tbm-lalu-to-ttr').val()) || 0;
			let tm_ini_to_ttr=parseInt($('#tm-ini-to-ttr').val()) || 0;
			let ttr_lalu=parseInt($('#ttr-lalu').val()) || 0;
			let ttr_ini_to_tbm =parseInt($('#ttr-ini-to-tbm').val()) || 0;
			
			let ttr_saat_ini = tbm_lalu_to_ttr + tm_ini_to_ttr + ttr_lalu - ttr_ini_to_tbm; 

			$('#ttr-saat-ini').val(ttr_saat_ini);
		})

		$('#jumlah').on('click',function(){
			let tbm_saat_ini=parseInt($('#tbm-saat-ini').val()) || 0;
			let tm_saat_ini=parseInt($('#tm-saat-ini').val()) || 0;
			let ttr_saat_ini=parseInt($('#ttr-saat-ini').val()) || 0;
			

			let jumlah = tbm_saat_ini + tm_saat_ini + ttr_saat_ini; 

			$('#jumlah').val(jumlah);
		})

		$('#ton').keyup(function(event) {
			let ton = parseInt($(this).val()) || 0;
			let tm_saat_ini=parseInt($('#tm-saat-ini').val()) || 0;

			let rata_rata = ton / tm_saat_ini * 1000;

			$('#rata-rata').val(parseInt(rata_rata));
		});

		$('#form-add-transaksi').submit(function(e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			submitTransaksi()
			// console.log('aa');
		});

		function submitTransaksi() {
			const button = $('.btn-simpan');
			let Form = $('#form-add-transaksi').serialize()+ "&csrf_hash_name=" + $('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url + "dns_perkebunan/transaksi/inserted",
				type: 'POST',
				data: Form,
				beforeSend: function() {
					button.text("Menyimpan..."); 
					button.attr("disabled", true); 
				},
				success: function(response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					// console.log(response)
					if (response.success) {
						Swal.fire({
							position: 'top-end',
							icon: 'success',
							title: 'Data Berhasil Disimpan',
							showConfirmButton: false,
							timer: 1500
						})
						$('#add-modal').modal('hide');
						Datatables();

					}
				},
				complete: function() {
					button.text("Simpan"); 
					button.attr("disabled", false); 
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}

		function dataKecamatan(){
			let opsi_kecamatan = '';
			opsi_kecamatan +='<option value="">Pilih Kecamatan</option>'
			$.ajax({
				url: base_url+'master/data-kecamatan',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kecamatan
						let name = response.data[key].nama_kecamatan
						opsi_kecamatan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kecamatan,#e-id_kecamatan').html(opsi_kecamatan);
				}
			});
		}

		function dataKomoditi(){
			let opsi_komoditi = '';
			opsi_komoditi +='<option value="">Pilih komoditi</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/komoditi/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_komoditi
						let name = response.data[key].nama_komoditi
						opsi_komoditi +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_komoditi,#e-id_komoditi').html(opsi_komoditi);
				}
			});
		}

		function dataKepemilikan(){
			let opsi_kepemilikan = '';
			opsi_kepemilikan +='<option value="">Pilih kepemilikan</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/kepemilikan/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_kepemilikan
						let name = response.data[key].kepemilikan+'-'+response.data[key].keterangan
						opsi_kepemilikan +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_kepemilikan,#e-id_kepemilikan').html(opsi_kepemilikan);
				}
			});
		}

		function dataPeriode(){
			let opsi_periode = '';
			opsi_periode +='<option value="">Pilih periode</option>'
			$.ajax({
				url: base_url+'dns_perkebunan/periode/databyid',
				type: 'GET',
				data: {
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				dataType:"json",
				success: function (response) {
					$('meta[name="csrf-token"]').attr("content", response.csrf_param);
					for(key in response.data){
						let id_ = response.data[key].id_periode
						let name = response.data[key].tahun+' : '+response.data[key].semester
						opsi_periode +='<option value="'+id_+'">'+name+'</option>'
					}

					$('#id_periode,#e-id_periode').html(opsi_periode);
				}
			});
		}

		function searchDataSmstlalu(){
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/data-smlalu',
				type: 'get',
				data: datas,
				success: function (response) {
					// console.log(response);
					if(stats == 'add'){
						if(response.success){
							$('#tbm-lalu').val(response.data['tbm']);
							$('#tm-lalu').val(response.data['tm']);
							$('#ttr-lalu').val(response.data['ttr']);

						}else{
							$('#form-add-transaksi input').removeAttr('disabled');
						}
						
					}else{
						$('#form-edit-transaksi input').removeAttr('disabled');
					}
					
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				}
			});
		}
		dataMaster();

		function dataMaster(){
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/databyid',
				type: 'get',
				data: {
					sids:id_trx,
					csrf_hash_name: $('meta[name="csrf-token"]').attr("content")
				},
				success: function (response) {
					if(response.data.kepemilikan.id_ != 4){
						table.column( 16 ).visible( false );
					}else{
						table.column( 16 ).visible( true );
					}
					$('#nama-komoditi').text(': '+response.data.komoditi.nama);
					$('#nama-kepemilikan').text(': '+response.data.kepemilikan.nama);
					$('#periode').text(': semester '+response.data.periode.semester+' Tahun '+response.data.periode.tahun);
				}
			});
		}


		function dataTransaksi(){
			datas['csrf_hash_name']=$('meta[name="csrf-token"]').attr("content");
			$.ajax({
				url: base_url+'dns_perkebunan/transaksi/data-trx',
				type: 'get',
				data: datas,
				success: function (response) {
					$('#tbm-lalu').val(response.data.edit.tbm_lalu);
					$('#tbm_lalu_to_tm').val(response.data.edit.tbm_ini_to_tm);
					$('#tbm_lalu_to_ttr').val(response.data.edit.tbm_ini_to_ttr);
					$('#tm-lalu').val(response.data.edit.tm_lalu);
					$('#tm-ini-to-ttr').val(response.data.edit.tbm_ini_to_ttr);
					$('#ttr-lalu').val(response.data.edit.ttr_lalu);
					$('#ttr-ini-to-tbm').val(response.data.edit.ttr_ini_to_tbm);
					$('#lahan-baru').val(response.data.lahan.luas_lahan);
					$('#tbm-saat-ini').val(response.data.tanaman.tbm);
					$('#tm-saat-ini').val(response.data.tanaman.tm);
					$('#ttr-saat-ini').val(response.data.tanaman.ttr);
					$('#jumlah').val(response.data.lahan.total);
					$('#ton').val(response.data.produksi.produksi);
					$('#rata-rata').val(response.data.produksi.rata_rata);
					$('#petani').val(response.data.petani.jumlah_kk);
				}
			});
		}



	})
function printDiv() 
{

	var divToPrint=document.getElementById('print-div');

	var newWin=window.open('','Print-Window');

	newWin.document.open();

	newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

	newWin.document.close();

	setTimeout(function(){newWin.close();},10);

}
</script>


@endsection