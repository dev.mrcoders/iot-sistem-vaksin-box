<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditi extends MY_Controller {
	private $data=[];
	private $nip;
	var $csrf_name = '';
	var $csrf_hash;
	var $json = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_komoditi');
		$this->csrf_name = 'csrf_param';
		$this->csrf_hash = $this->security->get_csrf_hash();
	}

	public function index()
	{
		$this->data['title'] = 'Ini Komoditi';
		$this->view('komoditi.index', $this->data);	
	}

	public function DataTables()
	{
		$list = $this->M_komoditi->get_datatables();
		$data = array();
		$no = $this->input->post('start');
		//looping data mahasiswa
		foreach ($list as $r) {
			$no++;
			$row = array();

			$row['no'] = $no;
			$row['nama'] = $r->nama_komoditi;
			$row['id_komoditi'] = $r->id_komoditi;

			$data[] = $row;
		}
		$this->json = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->M_komoditi->count_all(),
			"recordsFiltered" => $this->M_komoditi->count_filtered(),
			"data" => $data,
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
		
	}

	public function GetDataById()
	{

		$get_data_komoditi = $this->M_komoditi->GetById();

		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$get_data_komoditi
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Create()
	{
		$post=$this->input->post();

		if($this->input->post('id_komoditi')){
			$this->M_komoditi->Updated($post);
		}else{
			$this->M_komoditi->Saved($post);
		}
		

		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$post
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}

	public function Delete()
	{
		$sid = $this->input->post('sid');

		$this->M_komoditi->Deleted($sid);
		$this->json = array(
			"success" => true,
			"message" => 'ok',
			'data'=>$sid
		);
		$this->json[$this->csrf_name] = $this->csrf_hash;
		$this->output->set_content_type('application/json')->set_output(json_encode($this->json));
	}


}

/* End of file Samples.php */
		/* Location: ./application/modules/Dns_perikanan/controllers/Samples.php */