<?php

/* 
docs off route CodeIgniter Laravel-like static routes and filters
https://github.com/Patroklo/codeigniter-static-laravel-routes

BladeOne Blade Template Engine docs
https://github.com/EFTEC/BladeOne
*/

Route::filter('logged_in', function () {

	if ($this->session->userdata('login') == FALSE) {
		$this->session->set_flashdata('session', 'Session Sudah Habis Silahkan Login');
		redirect(base_url());
	}
});


Route::get('default_controller', 'website');




