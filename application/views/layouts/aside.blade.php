<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ base_url('home') }}" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <?php 
                $nip = $this->CI->session->userdata('nip');
                $role = $this->CI->access->getrole($nip)->id_role;
                $this->CI->db->select('b.keterangan,b.nama_modules,a.id_modules');
                $this->CI->db->join('tb_modules b','a.id_modules=b.id_modules','inner');
                $module = $this->CI->db->get_where('tb_modules_role a', ['a.id_role'=>$role])->result();

                ?>
                @foreach($module as $modules)
                <li class="nav-small-cap">
                    <i class="mdi mdi-view-module"></i>
                    <span class="hide-menu">{{ $modules->keterangan }}</span>
                </li>
                <?php $menu = $this->CI->db->get_where('v_menu',['id_modules'=>$modules->id_modules])->result() ?>
                @foreach($menu as $menus)
                <?php $submenu = $this->CI->db->get_where('v_submenu_menu',['id_menu'=>$menus->id_menu])->result() ?>
                @if($submenu)
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">{{ $menus->nama_menu }} </span>
                    </a>
                    <ul aria-expanded="false" class="collapse first-level">
                        @foreach($submenu as $submenus)
                        <li class="sidebar-item">
                            <a href="{{ base_url($modules->nama_modules) }}" class="sidebar-link">
                                <i class="mdi mdi-email"></i>
                                <span class="hide-menu"> {{ $submenus->nama_submenu }} </span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @else
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ base_url($modules->nama_modules) }}" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">{{ $menus->nama_menu }}</span>
                    </a>
                </li>
                @endif
                @endforeach
                @endforeach
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>