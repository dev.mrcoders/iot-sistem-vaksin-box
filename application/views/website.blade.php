
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Kapella Bootstrap Admin Dashboard Template</title>
	<!-- base:css -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="@asset('assets/')vendors/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="@asset('assets/')vendors/base/vendor.bundle.base.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="@asset('assets/')css/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="images/favicon.png" />
</head>
<body>
	<div class="container-scroller">
		
		<!-- partial:partials/_horizontal-navbar.html -->
		<div class="horizontal-menu">
			<nav class="navbar top-navbar col-lg-12 col-12 p-0">
				<div class="container-fluid">
					<div class="navbar-menu-wrapper d-flex align-items-center justify-content-between">
						
						<div class="navbar-nav navbar-nav-left">
							<h3 class="text-secondary font-weight-bold" id="tanggal"></h3>
						</div>
						<div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
							<label for="" class="navbar-brand brand-logo text-secondary text-uppercase font-weight-bold">ALARM SUHU KRITIS COLD CHAIN RS AWAL BROSS PEKANBARU</label>
							<label for="" class="navbar-brand brand-logo-mini text-secondary text-uppercase font-weight-bold">IOT RS</label>
							
						</div>
						<div class="navbar-nav navbar-nav-right ">
							<h3 class="text-secondary text-uppercase font-weight-bold" id="jam"></h3>
						</div>
						
					</div>
				</div>
			</nav>
			
		</div>
		<!-- partial -->
		<div class="container-fluid page-body-wrapper">
			<div class="main-panel">
				<div class="content-wrapper">
					<div class="row">
						@for($a = 1;$a<=8;$a++)
						<div class="col-sm-3 flex-column d-flex stretch-card mt-1 ">
							<div class="row flex-grow">
								<div class="col-sm-12 grid-margin stretch-card">
									<div class="card " id="bg-card">
										<div class="card-body">
											<div class="row">
												<div class="col-lg-8 ">
													<h3 class="font-weight-bold text-dark">Vaksin Box {{ $a }}</h3>

													<div class="d-lg-flex align-items-baseline mb-3">
														<h1 class="text-dark font-weight-bold">23<sup class=""><span>&#8451;</span></sup></h1>
														
													</div>
												</div>
												<div class="col-lg-4">
													<div class="position-relative">
														
														<div class="live-info badge badge-success">Live</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-12 mt-4 mt-lg-0">
													<div class="bg-primary text-white px-2 py-2 card">
														<div class="row">
															<div class="col-sm-6">
																<h2>23.00<sup class="font-weight-medium"><span>&#8451;</span></sup></h2>
																<p class="mb-0">TEMP C</p>
															</div>
															<div class="col-sm-6 climate-info-border mt-lg-0 mt-2">
																<h2>15.00<sup class="font-weight-light"><small class="font-weight-medium"> F</small></sup></h2>
																<p class="mb-0">TEMP F</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row pt-3 mt-md-1">
												<div id="chartContainer{{ $a }}" style="height: 100px; width: 90%;"></div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
						@endfor
					</div>

				</div>
				
				<footer class="footer">
					<div class="footer-wrap">
						<div class="d-sm-flex justify-content-center justify-content-sm-between">
							<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <a href="https://www.bootstrapdash.com/" target="_blank">bootstrapdash.com </a>2021</span>
							<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Only the best <a href="https://www.bootstrapdash.com/" target="_blank"> Bootstrap dashboard </a> templates</span>
						</div>
					</div>
				</footer>
				<!-- partial -->
			</div>
			<!-- main-panel ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->
	<!-- base:js -->
	<script src="@asset('assets/')vendors/base/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- Plugin js for this page-->
	<!-- End plugin js for this page-->
	<!-- inject:js -->
	<!-- <script src="@asset('assets/')js/template.js"></script> -->
	<!-- endinject -->
	<!-- plugin js for this page -->
	<!-- End plugin js for this page -->
	<script src="@asset('assets/')vendors/chart.js/Chart.min.js"></script>
	<script src="@asset('assets/')vendors/progressbar.js/progressbar.min.js"></script>
	<script src="@asset('assets/')vendors/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js"></script>
	<script src="@asset('assets/')vendors/justgage/raphael-2.1.4.min.js"></script>
	<script src="@asset('assets/')vendors/justgage/justgage.js"></script>
	<script src="@asset('assets/')js/jquery.cookie.js" type="text/javascript"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	<!-- Custom js for this page-->
	<!-- End custom js for this page-->
	
	<script>
		$(function(){
			dateTime();

			if ($('#circleProgress1').length) {
				var bar = new ProgressBar.Circle(circleProgress1, {
					color: '#0aadfe',
					strokeWidth: 10,
					trailWidth: 10,
					easing: 'easeInOut',
					duration: 1400,
					width: 42,
				});
				bar.animate(.18); 
			}
			if ($('#circleProgress2').length) {
				var bar = new ProgressBar.Circle(circleProgress2, {
					color: '#fa424a',
					strokeWidth: 10,
					trailWidth: 10,
					easing: 'easeInOut',
					duration: 1400,
					width: 42,

				});
				bar.animate(1.0); 
			}
			for (var i = 1; i <=8; ++i) {
				this["chart"+i] = new CanvasJS.Chart("chartContainer"+i, {
					animationEnabled: true,
					theme: "light2",
					title:{
						text: "Simple Line Chart"
					},
					data: [{        
						type: "line",
						indexLabelFontSize: 16,
						dataPoints: [
							{ y: 450 },
							{ y: 414},
							{ y: 520 },
							{ y: 460 },
							{ y: 450 },
							{ y: 500 },
							{ y: 480 },
							{ y: 480 },
							{ y: 410 },
							{ y: 500 },
							{ y: 480 },
							{ y: 510 }
							]
					}]
				});

				this["chart"+i].render();
			}
			


			
			// chart.render();
			// chart2.render();
		})

		// var $el = $("#bg-card"),
		// x = 5000,
		// originalColor = $el.css("background");

		// $el.removeClass('bg-danger');
		// setInterval(function(){
		// 	$el.addClass('bg-danger');
		// }, x);

		// setInterval(function () {

		// 	$el.removeClass('bg-danger');
		// 	setTimeout(function(){
		// 		$el.addClass('bg-danger');
		// 	}, x);
		

		// }, 3000);

		setInterval(function () {

			dateTime();


		}, 1000);
	</script>
	<script src="@asset('assets/')js/date-time.js" type="text/javascript"></script>
</body>
</html>